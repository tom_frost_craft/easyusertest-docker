
### Bootstrap process
- Install docker - https://www.docker.com 
- Go to docker folder - `cd docker` (all commands run from docker folder only)
- Run docker - `docker-compose up -d --build apache2 mysql workspace` (for the first time it will take some time)
- Add to Hosts File -> 127.0.0.1   tests.loc
- http://tests.loc/ || https://tests.loc/
- Enjoy your life ))

#### Project setup
- Run command - `docker-compose stop`
- Replace `www` folder with your project
- Setup project .env file
    - DB_HOST=mysql
    - DB_PORT=3306
    - DB_DATABASE=default
    - DB_USERNAME=root
    - DB_PASSWORD=root
- Run bash - `docker-compose exec workspace bash`
- Run commands for project setup (composer, artisan)

#### Commands list 
** Run docker ** - `docker-compose up -d --build apache2 mysql workspace`

** Stop docker ** - `docker-compose stop`

** Run bash ** - `docker-compose exec workspace bash`

#### How connect to DB (Sequel Pro)
- host - 127.0.0.1
- user - root
- pass - root
